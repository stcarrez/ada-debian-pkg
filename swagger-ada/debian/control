Source: libada-swagger
Priority: optional
Section: libdevel
Maintainer: Stephane Carrez <Stephane.Carrez@gmail.com>
Build-Depends: debhelper (>= 10), autotools-dev,
 gnat, gnat-7,
# This line is parsed by debian/rules.
 gprbuild (>= 2015-2)
# 2015-2 is compatible with gnat-6
Standards-Version: 4.1.4
Homepage: https://github.com/stcarrez/swagger-ada/
Vcs-Git: https://github.com/stcarrez/ada-debian-pkg.git
Vcs-Browser: https://github.com/stcarrez/ada-debian-pkg

Package: libada-swagger0.1.0
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Swagger/OpenAPI Ada Client Support (runtime)
 The Swagger Ada library is a small support library for the Ada code
 generator provided by Swagger Codegen.  The library provides support to
 serialize the data, make HTTP requests and support the OpenAPI Spec
 (https://github.com/OAI/OpenAPI-Specification).
 .
 This package contains the runtime client libraries.

Package: libada-swagger-server0.1.0
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Swagger/OpenAPI Ada Server Support (runtime)
 The Swagger Ada library is a small support library for the Ada code
 generator provided by Swagger Codegen.  The library provides support to
 serialize the data, make HTTP requests and support the OpenAPI Spec
 (https://github.com/OAI/OpenAPI-Specification).
 .
 This package contains the runtime server libraries.

Package: libada-swagger7-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libada-swagger0.1.0 (= ${binary:Version}),
 libada-util7-dev,
 libada-util7-http-dev,
 gnat
Description: Swagger/OpenAPI Ada Client Support (development)
 The Swagger Ada library is a small support library for the Ada code
 generator provided by Swagger Codegen.  The library provides support to
 serialize the data, make HTTP requests and support the OpenAPI Spec
 (https://github.com/OAI/OpenAPI-Specification).
 .
 This package contains the development files for Swagger Ada clients.

Package: libada-swagger-server7-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libada-swagger0.1.0 (= ${binary:Version}),
 libada-swagger-server0.1.0 (= ${binary:Version}),
 libada-servlet7-dev,
 gnat
Description: Swagger/OpenAPI Ada Server Support (development)
 The Swagger Ada library is a small support library for the Ada code
 generator provided by Swagger Codegen.  The library provides support to
 serialize the data, make HTTP requests and support the OpenAPI Spec
 (https://github.com/OAI/OpenAPI-Specification).
 .
 This package contains the development files for Swagger Ada servers.
